import React from 'react';
import './App.css';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    Redirect
} from "react-router-dom";
import MyPodcast from '../src/pages/MyPodcastPage/MyPodcast';
import HomePage from './pages/Home/HomePage';
import LoginPage from './pages/LoginPage/LoginPage';
import MyPodcastForm from './pages/MyPodcastFormPage/MyPodcastForm';
import MyPodcastLibraryPage from './pages/MyPodcastLibrary/MyPodcastLibraryPage';
import RatePage from './pages/RatePage/RatePage';

function App() {
  return (
    <Router>
    
       

            <Switch>
                 <Route path="/home">
                    <HomePage />
                </Route>
                <Route path="/podcasts">
                    <MyPodcast />
                </Route>
                <Route path="/login">
                    <LoginPage />
                </Route>
                <Route path="/create">
                    <MyPodcastForm />
                </Route>
                <Route path="/gallery">
                    <MyPodcastLibraryPage/>
                </Route>
                <Route path="/rate">
                    <RatePage/>
                </Route>
                
               
            </Switch>
        
    

</Router>
  );
}

export default App;