import React, {useState, useRef} from 'react';
import { useForm } from "react-hook-form";
import './MyPodcastForm.scss';
import { Link } from 'react-router-dom';
import {API} from '../../shared/services/api' ;



export default function MyPodcastForm() {
   
    const { register, handleSubmit, errors} = useForm();
    const refForm =  useRef(null);
    
   
      const onSubmit = (data) => {
        console.log(data);
        const formData = new FormData(refForm.current);

        API.post('podcasts', formData)
        .then(res => {
            console.log(res.formData);
            window.location.href = "/rate";
            
        }).catch((error) => {
            alert('Los campos son incorrectos.')
        })

       
    }
   

    

    return (
    
        <div>
            <div className="c-backgroundImgForm"></div>
        <div className="c-title">
        
                <h2>¡Crea tu podcast!</h2>
        
                <p>Empieza a guardar tu propia música</p>
            </div>
        <form onSubmit={handleSubmit(onSubmit)} className="c-registerForm" ref={refForm} >
               
                <label htmlFor="title">
                    <input type="text" className="c-registerForm__inputTitle" name="title" id="title" placeholder="Titulo"
                           ref={register ({ required: true })} />
                           {errors.title && <span className="c-registerForm__span"> El campo título es requerido</span>}
                </label>
                {/* <label htmlFor="image">
                    <input type="text" ref={register}  className="c-registerForm__inputImages" name="image" id="image" placeholder="Categoria"
                       />

                </label> */}
               
                <button className="b-main-button b-main-button__blue">Guardar Podcast</button>
            </form>

            <footer>
                <ul className="menu-bottons">
                <Link to="/podcasts"><span className="top-links-register fas fa-arrow-left"></span></Link>
                <Link to="/gallery"><span className="top-links-register fas fa-headphones"></span></Link>
                <Link to="/home"><span className="top-links-register fas fa-home"></span></Link> 
                </ul>
            </footer>

        </div>

    );

}

