import React, { useEffect, useState } from "react";
import "./MyPodcastLibraryPage.scss"
import {Link} from "react-router-dom";



export default function MyPodcastLibraryPage() {


return(

    <div>

        <div className="c-backgroundImgLibrary">
            </div>
            <div className="c-title">
                <h2>¡Descubre el mundo podcast!</h2>
            </div>


                     <div className="c-resultspage">
                        <div className="row">
                            <div className="col-lg-3 col-md-2 col-sm-12">
                                <h2 className="c-resultspage__msg">El 2020 en podcasts</h2>
                                    <div className="c-resultspage__product">
                                        <div className="b-frame__green">
                                            <img className="b-img-frame" src="https://images.unsplash.com/photo-1493225457124-a3eb161ffa5f?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1350&q=80" alt=""/>
                                            <p className="b-frame__caption">Joe Rogan Experience</p>
                                        </div>
                                        <div className="b-frame__green">
                                        <img className ="b-img-frame" src="https://images.unsplash.com/photo-1483412033650-1015ddeb83d1?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1353&q=80" alt=""/>
                                        <p className="b-frame__caption">Call Her Daddy</p>
                                        </div>
                                        <div className="b-frame__green">
                                        <img className ="b-img-frame" src="https://images.unsplash.com/photo-1598369621747-83595bf3e687?ixid=MXwxMjA3fDB8MHxzZWFyY2h8NDJ8fHBvZGNhc3R8ZW58MHx8MHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60" alt=""/>
                                        <p className="b-frame__caption">Michelle Obama Podcast</p>
                                        </div>
                                        <div className="b-frame__green ">
                                        <img className ="b-img-frame" src="https://images.unsplash.com/photo-1483691278019-cb7253bee49f?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1350&q=80" alt=""/>
                                        <p className="b-frame__caption">Nadie sabe nada</p>
                                        </div>
                                        
                                        
                                
                                    </div>
                        
                        
                                <h2 className="c-resultspage__msg">Últimas tendencias</h2>
                                    <div className="c-resultspage__product">
                                        <div className="b-frame__green">
                                        <img className="b-img-frame" src="https://images.unsplash.com/photo-1494500764479-0c8f2919a3d8?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1350&q=80" alt=""/>
                                        <p className="b-frame__caption">Entiende tu mente</p>
                                        </div>
                                        <div className="b-frame__green">
                                        <img className="b-img-frame" src="https://images.unsplash.com/photo-1531816247963-c7b28072d65d?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1350&q=80" alt=""/>
                                        <p className="b-frame__caption">Meditada</p>
                                        </div>
                                        <div className="b-frame__green">
                                        <img className="b-img-frame" src="https://images.unsplash.com/photo-1574055357184-96c00ccb5e6d?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1350&q=80" alt=""/>
                                        <p className="b-frame__caption">La vida moderna</p>
                                        </div>
                                        <div className="b-frame__green">
                                        <img className="b-img-frame" src="https://images.unsplash.com/photo-1418065460487-3e41a6c84dc5?ixid=MXwxMjA3fDB8MHxzZWFyY2h8N3x8bmF0dXJlfGVufDB8fDB8&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60" alt=""/>
                                        <p className="b-frame__caption">Favourite murder</p>
                                        </div>
                                
                                    </div>
        

            
                                <h2 className="c-resultspage__msg">Pon un podcast en tú vida</h2>
                                    <div className="c-resultspage__product">
                                        <div className="b-frame__green">
                                        <img className="b-img-frame" src="https://images.unsplash.com/photo-1557531110-2a3cafdf2ac4?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1350&q=80" alt=""/>
                                        <p className="b-frame__caption">Girlboss</p>
                                        </div>
                                        <div className="b-frame__green">
                                        <img className="b-img-frame" src="https://images.unsplash.com/photo-1604537529428-15bcbeecfe4d?ixid=MXwxMjA3fDF8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1349&q=80" alt=""/>
                                        <p className="b-frame__caption">Viajero Occidental</p>
                                        </div>
                                        <div className="b-frame__green">
                                        <img className="b-img-frame" src="https://images.unsplash.com/photo-1550273757-37b28a257de2?ixid=MXwxMjA3fDB8MHxzZWFyY2h8Mnx8ZGlvcnxlbnwwfHwwfA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60" alt=""/>
                                        <p className="b-frame__caption">Dior Talks</p>
                                        </div>
                                        <div className="b-frame__green">
                                        <img className="b-img-frame" src="https://images.unsplash.com/photo-1585829365295-ab7cd400c167?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1350&q=80" alt=""/>
                                        <p className="b-frame__caption">The Daily</p>
                                        </div>
                                    </div>

                                    <h2 className="c-resultspage__msg">Pensado para foodies</h2>
                                    <div className="c-resultspage__product">
                                        <div className="b-frame__green">
                                        <img className="b-img-frame" src="https://images.unsplash.com/photo-1499881696443-e66dd9a7b496?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1351&q=80" alt=""/>
                                        <p className="b-frame__caption">Cocina Saludable</p>
                                        </div>
                                        <div className="b-frame__green">
                                        <img className="b-img-frame" src="https://images.unsplash.com/photo-1554374814-6f1baaa46062?ixid=MXwxMjA3fDB8MHxzZWFyY2h8Njh8fGZvb2RpZXN8ZW58MHx8MHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60" alt=""/>
                                        <p className="b-frame__caption">Veggies</p>
                                        </div>
                                        <div className="b-frame__green">
                                        <img className="b-img-frame" src="https://images.unsplash.com/photo-1529692236671-f1f6cf9683ba?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1350&q=80" alt=""/>
                                        <p className="b-frame__caption">Échame el diente</p>
                                        </div>
                                        <div className="b-frame__green">
                                        <img className="b-img-frame" src="https://images.unsplash.com/photo-1592558480962-903b06077406?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1343&q=80" alt="" />
                                        <p className="b-frame__caption">Eat Local</p>
                                        </div>
                                    </div>
                                  
                                    
                            </div>
                        </div>
                    </div>

                <footer>
                <ul className="menu-bottons-library">
                <Link to="/login"><span className="top-links-register fas fa-user"></span></Link>
                <Link to="/home"><span className="top-links-register fas fa-home"></span></Link> 
                </ul>
            </footer>

    </div>
)

}