import axios from 'axios';
import React, { useEffect, useState } from 'react';
import MyPodcastGallery from '../Components/MyPodcastGallery';
import PodcastSearch from '../Components/PodcastSearch/PodcastSearch';
import {API} from '../../shared/services/api';


let podcasts = [];

export default function MyPodcastPage() {

    const [filteredPodcasts, setFilteredPodcasts] = useState([]);

    useEffect(() => {

       
       
            axios.get(process.env.REACT_APP_BACK_URL +'podcasts').then(res => {
                const podcastsLocal = res.data;
                console.log(res);
                podcasts = podcastsLocal; 
                console.log(podcastsLocal);
                setFilteredPodcasts(podcastsLocal);
            })

    

    }, [])

 
    

    const filterPodcasts = (filterValues) => {
        console.log(filterValues);
        const filteredLocalPodcasts = [];

        for (const podcast of podcasts ) {
            if (podcast.title.includes(filterValues.title)) {
                filteredLocalPodcasts.push(podcast);
            }
        }

        setFilteredPodcasts(filteredLocalPodcasts);

    }



    return (

        <div>
            
            <PodcastSearch fnSubmit={filterPodcasts} />
            <MyPodcastGallery podcasts={filteredPodcasts} />
        </div>
    );

}