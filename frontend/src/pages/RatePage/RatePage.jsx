import React from "react";
import { Link } from "react-router-dom";

import "./RatePage.scss";

export default function RatePage() {
    return (
        <div>
            <div className="rate-page-container">

                <h3 className="rate-page-container__text">
                    ¡Gracias por usar MyPodcast!
                </h3>
                <h3 className="rate-page-container__subtitle">
                    Por favor, evalua tu experiencia.
                </h3>
                <div className="rating">
                    <span>☆</span>
                    <span>☆</span>
                    <span>☆</span>
                    <span>☆</span>
                    <span>☆</span>
                </div>
                <a href="/podcasts" className="rate-page-container__suggestions">Enviar Sugerencias</a>
            </div>
        </div>
    );
}
