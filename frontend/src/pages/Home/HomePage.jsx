import React from "react";
import "./HomePage.scss";
import { Link } from 'react-router-dom';
export default function HomePage() {
    return (
        <div>
            
            <div className="c-title">
                <h1>MyPodcast</h1>
                <h4>Tu guía para escuchar música</h4>
            </div>
            <div className="c-backgroundWelcome">
            </div>
            <footer className="c-footer">
            <Link to="/gallery" className="c-footer__link">Descubre el mundo MyPodcast</Link>
            <Link to="/login" className="c-footer__backHome">Tus podcasts</Link>
            
            </footer>
        </div>

    );
}
