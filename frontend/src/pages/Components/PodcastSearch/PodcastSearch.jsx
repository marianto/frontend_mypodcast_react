import React from 'react';
import './PodcastSearch.scss'
import { useForm } from "react-hook-form";
import { Link } from 'react-router-dom';


export default function PodcastSearch (props) {

    const { register, handleSubmit} = useForm();


    const fnChange = (data) => {
        props.fnSubmit(data)
    };

    return (
    <div>
        <div className="header-bottons">
        <Link to="/login"><span className="top-links-register fas fa-arrow-left"></span></Link>
        <Link to="/create"><span className="top-links-register fas fa-arrow-right"></span></Link>
        </div>
        <form className="search-form" onSubmit={handleSubmit(fnChange)}>
            <label htmlFor="title">
                <span className="b-text-label"></span>
                <input className="search-form_input" placeholder="Encuentra tu podcast..." onChange={handleSubmit(fnChange)} name="title" id="title"
                       ref={register}/>
            </label>
        </form>
    </div>
    )
}