import React, {useState} from 'react';
import './MyPodcastGallery.scss';
import { Link } from 'react-router-dom';
import { API } from '../../shared/services/api';


export default function MyPodcastGallery(props) {

    const deletePodcast = (id)=> {
        
        API.delete('podcasts/' + id)
        .then(res => {
            console.log(res.id);
            alert('tu podcast ha sido borrado');
            
            
        }).catch((error) => {
            console.log('error');
        })
    };



    return (
    
    <div>
        <div className="c-podcast-gallery">
            <div className="row">
                {props.podcasts.map((item) =>
                    <div className="col-12 col-md-6 col-lg-4" key={item.id}>
                            <div className="c-podcast-gallery__item-container">

                                <div className="c-podcast-gallery__item">
                                    <img className="c-podcast-gallery__img" src={item.image} alt=""></img>
                                    <p className="c-podcast-gallery__caption">{item.title}
                                    <br/>
                                    <span onClick={()=>deletePodcast(item.id)} className="c-podcast-gallery__span fas fa-trash" ></span>
                                    <span className="c-podcast-gallery__span_heart fas fa-heart"></span>
                                    </p>
                                    {/* <p className="c-podcast-gallery__caption overlay">{item.categories}</p> */}
    
                                </div>

                            </div>
                       
                    </div>

                )}
                
            </div>
        </div>
        </div>
    )
}